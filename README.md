# Tex

This is LaTeX primer for beginners or intermediate users.

## How to use

1. see [tex.pdf](https://gitlab.com/imn1/tex/-/blob/master/tex.pdf?ref_type=heads)
   for the output
2. the sections are separated into standalone files with `_` prefix
3. see each standalone file for details related to the section
4. download, read, try and use
